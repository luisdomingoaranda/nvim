-- :help options
vim.opt.backup = false                           -- creates a backup file
vim.opt.clipboard = "unnamedplus"                -- allows neovim to access the system clipboard
vim.opt.cmdheight = 2                            -- more space in the neovim command line for displaying messages
vim.opt.completeopt = { "menuone", "noselect" }  -- mostly just for cmp
vim.opt.conceallevel = 0                         -- so that `` is visible in markdown files
vim.opt.fileencoding = "utf-8"                   -- the encoding written to a file
vim.opt.hlsearch = true                          -- highlight all matches on previous search pattern
vim.opt.ignorecase = true                        -- ignore case in search patterns
vim.opt.mouse = "a"                              -- allow the mouse to be used in neovim
vim.opt.pumheight = 10                           -- pop up menu height
vim.opt.showmode = false                         -- we don't need to see things like -- INSERT -- anymore
vim.opt.smartcase = true                         -- smart case
vim.opt.splitbelow = true                        -- force all horizontal splits to go below current window
vim.opt.splitright = true                        -- force all vertical splits to go to the right of current window
vim.opt.swapfile = false                         -- creates a swapfile
vim.opt.timeoutlen = 500                         -- time to wait for a mapped sequence to complete (in milliseconds)
vim.opt.undofile = true                          -- enable persistent undo
vim.opt.updatetime = 300                         -- faster completion (4000ms default)
vim.opt.writebackup = false                      -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
vim.opt.showtabline = 1                          -- 1: show tabs when more than 1 tab, 2: always show tabs
vim.opt.cursorline = true                        -- highlight the current line
vim.opt.number = true                            -- set numbered lines
vim.opt.relativenumber = true                    -- set relative numbered lines
vim.opt.numberwidth = 4                          -- set number column width to 2 {default 4}
vim.opt.signcolumn = "yes"                       -- always show the sign column, otherwise it would shift the text each time
-- vim.opt.colorcolumn = "81"                       -- Show colored column 81
vim.opt.termguicolors = true                     -- set term gui colors (most terminals support this)
vim.opt.shell = "zsh"
vim.opt.wrap = false                             -- display lines as one long line
vim.opt.scrolloff = 999                          -- cursor is vertically centered
vim.opt.sidescrolloff = 1
vim.opt.guifont = "monospace:h17"                -- the font used in graphical neovim applications
vim.opt.shortmess:append "c"
vim.opt.list = true                              -- show some hidden characters
vim.opt.listchars = {
  eol = '󰌑',
  tab = ' ',
  trail = '·'
}

-- Status bar
-- vim.opt.laststatus = 3                           -- 3 -> global statusbar
-- Remove winseparator
-- vim.cmd("highlight WinSeparator guibg=None")
-- vim.api.nvim_set_hl(0, 'Winseparator', { fg = '#ffffff', bg = '#ffffff' }) -- same as above?

-- default indentation settings
vim.opt.expandtab = true                         -- convert tabs to spaces
vim.opt.smartindent = true                       -- make indenting smarter again
vim.opt.shiftwidth = 2                           -- size of an indentation
vim.opt.tabstop = 2                              -- insert n spaces for a tab
vim.opt.softtabstop = 2                          -- number of spaces a <Tab> counts for. When 0, feature is off

vim.cmd("set whichwrap+=<,>,[,],h,l")
vim.cmd([[set iskeyword+=-]])
