local utils = require("config.utils")

-- Install (not really, because it comes with neovim)
vim.cmd("packadd termdebug")

-- Options
vim.g.termdebugger = os.getenv("GDB")
vim.g.termdebug_popup = 0
vim.g.termdebug_wide = 163

-- Keymaps
utils.keymap_set("n", "<leader>uo", function() vim.cmd("Termdebug") end, { desc = "Open termdebug" })
utils.keymap_set("n", "<leader>ub", function() vim.cmd("Break") end, { desc = "Add breakpoint" })
utils.keymap_set("n", "<leader>ua", function() vim.cmd("Clear") end, { desc = "Clear breakpoint" })
utils.keymap_set("n", "<leader>uc", function() vim.cmd("Continue") end, { desc = "Continue" })
utils.keymap_set("n", "<leader>us", function() vim.cmd("Step") end, { desc = "Step" })
utils.keymap_set("n", "<leader>un", function() vim.cmd("Over") end, { desc = "Next" })
