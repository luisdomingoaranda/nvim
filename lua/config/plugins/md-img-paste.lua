require("config.utils").keymap_set(
  "n",
  "<leader>mp",
  ":call mdip#MarkdownClipboardImage()<cr>",
  { desc = "Markdown paste image" }
)
