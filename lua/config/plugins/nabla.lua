local utils = require("config.utils")

utils.keymap_set(
  "n",
  "<leader>mf",
  ":lua require('nabla').popup()<CR>",
  { desc = "see latex function" }
)
