local utils = require("config.utils")

require("gitsigns").setup({
  signs = {
    add          = { text = '┃' },
    change       = { text = '┃' },
    delete       = { text = '_' },
    topdelete    = { text = '‾' },
    changedelete = { text = '~' },
    untracked    = { text = '┆' },
  },
  signcolumn = true, -- Toggle with `:Gitsigns toggle_signs`
  numhl = false, -- Toggle with `:Gitsigns toggle_numhl`
  linehl = false, -- Toggle with `:Gitsigns toggle_linehl`
  word_diff = false, -- Toggle with `:Gitsigns toggle_word_diff`
  watch_gitdir = {
    interval = 1000,
    follow_files = true,
  },
  attach_to_untracked = true,
  current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
  current_line_blame_opts = {
    virt_text = true,
    virt_text_pos = "eol", -- 'eol' | 'overlay' | 'right_align'
    delay = 1000,
    ignore_whitespace = false,
  },
  sign_priority = 6,
  update_debounce = 100,
  status_formatter = nil, -- Use default
  max_file_length = 40000,
  preview_config = {
    -- Options passed to nvim_open_win
    border = "single",
    style = "minimal",
    relative = "cursor",
    row = 0,
    col = 1,
  },
})

-- Keymaps
utils.keymap_set("n", "<leader>gj", ":Gitsigns next_hunk<cr>", { desc = "Next" })
utils.keymap_set("n", "<leader>gk", ":Gitsigns prev_hunk<cr>", { desc = "Previous" })
utils.keymap_set("n", "<leader>gl", ":Gitsigns blame_line<cr>", { desc = "Blame line" })
utils.keymap_set("n", "<leader>gp", ":Gitsigns preview_hunk<cr>", { desc = "Preview" })
utils.keymap_set("n", "<leader>gr", ":Gitsigns reset_hunk<cr>", { desc = "Reset" })
utils.keymap_set("n", "<leader>gR", ":Gitsigns reset_buffer<cr>", { desc = "Reset buffer" })
utils.keymap_set("n", "<leader>gs", ":Gitsigns stage_hunk<cr>", { desc = "Stage" })
utils.keymap_set("n", "<leader>gu", ":Gitsigns undo_stage_hunk<CR>", { desc = "Unstage" })
utils.keymap_set("n", "<leader>gd", ":Gitsigns diffthis HEAD<cr>", { desc = "Diff" })
