local utils = require("config.utils")

-- Automatically install packer
local install_path = vim.fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = vim.fn.system({
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  })
  print("Installing packer close and reopen Neovim...")
  vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost init.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

packer.init({
  -- Avoid saving files in the store directory (nix)
  compile_path = vim.fn.stdpath('data') .. "/site/plugin/packer_compiled.lua",
  -- Have packer use a popup window
  display = {
    open_fn = function()
      return require("packer.util").float({ border = "rounded" })
    end,
  },
})

-- Mappings
utils.keymap_set("n", "<leader>pc", "<cmd>PackerCompile<cr>")
utils.keymap_set("n", "<leader>pi", "<cmd>PackerInstall<cr>")
utils.keymap_set("n", "<leader>ps", "<cmd>PackerSync<cr>")
utils.keymap_set("n", "<leader>pS", "<cmd>PackerStatus<cr>")
utils.keymap_set("n", "<leader>pu", "<cmd>PackerUpdate<cr>")


-- Install your plugins here
return packer.startup(function(use)
  -- Basics
  use("wbthomason/packer.nvim") -- Have packer manage itself
  use("nvim-lua/popup.nvim") -- An implementation of the Popup API from vim in Neovim
  use("nvim-lua/plenary.nvim") -- Useful lua functions used ny lots of plugins
  use("jiangmiao/auto-pairs") -- (), [], {}, "", ''
  use("tpope/vim-obsession") -- Automatically manage session files

  -- Colorschemes
  use("Th3Whit3Wolf/space-nvim")
  use("luisiacc/gruvbox-baby")
  use("bluz71/vim-moonfly-colors")
  use("shaeinst/roshnivim-cs")
  use("folke/tokyonight.nvim")
  use("sainnhe/everforest")
  use("rockerBOO/boo-colorscheme-nvim")
  use({ "catppuccin/nvim", as = "catppuccin" })
  use("sam4llis/nvim-tundra")
  use("Everblush/everblush.vim")
  use("navarasu/onedark.nvim")
  use("rebelot/kanagawa.nvim")
  use("mhartington/oceanic-next")

  -- Comments
  use({
    "numToStr/Comment.nvim",
    config = function() require("Comment").setup() end
  })

  -- Surrounds
  use({
    "kylechui/nvim-surround",
    config = function() require("nvim-surround").setup() end
  })

  -- Auto indent settings
  use({
    "nmac427/guess-indent.nvim",
    config = function() require("guess-indent").setup({}) end
  })

  -- Telescope
  use({
    "nvim-telescope/telescope.nvim",
    config = function() require("config.plugins.telescope") end,
    requires = {
      "nvim-telescope/telescope-dap.nvim"
    }
  })

  -- Treesitter
  use({
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
    config = function() require("config.plugins.treesitter.treesitter") end,
    requires = {
      "nvim-treesitter/nvim-treesitter-textobjects",
      config = function() require("config.plugins.treesitter.textobjects") end,
    }
  })

  -- Completion
  use({
    "neovim/nvim-lspconfig",
    config = function() require("config.plugins.lsp") end,
    requires = {
      "hrsh7th/nvim-cmp",
      config = function() require("config.plugins.cmp") end,
      requires = {
        "hrsh7th/nvim-lspconfig",
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-path",
        "hrsh7th/cmp-cmdline",
        {
          -- Snippets
          "L3MON4D3/LuaSnip",
          config = function() require("config.plugins.luasnip") end,
          requires = {
            "saadparwaiz1/cmp_luasnip",
            "rafamadriz/friendly-snippets",
          }
        }
      }
    }
  })

  -- Debuggers
  use({
    "rcarriga/nvim-dap-ui",
    config = function() require("config.plugins.nvim-dap.nvim-dap-ui") end,
    requires = {
      "mfussenegger/nvim-dap",
      config = function() require("config.plugins.nvim-dap") end,
      requires = {
        "Iron-E/nvim-libmodal", -- to setup debug mode
        "nvim-neotest/nvim-nio",
      }
    },
  })

  -- null-ls
  use({
    "jose-elias-alvarez/null-ls.nvim",
    config = function() require("config.plugins.null-ls") end
  })

  -- Which-key
  use({
    "folke/which-key.nvim",
    config = function() require("config.plugins.which-key") end
  })

  -- Treeview
  use({
    "kyazdani42/nvim-tree.lua",
    config = function() require("config.plugins.nvim-tree") end,
    requires = { "kyazdani42/nvim-web-devicons" }
  })

  -- Bufferline
  -- use({
  --   "akinsho/bufferline.nvim",
  --   config = function() require("bufferline").setup() end,
  --   requires = {'kyazdani42/nvim-web-devicons'},
  -- })

  -- Git signs
  use({
    "lewis6991/gitsigns.nvim",
    config = function() require("config.plugins.gitsigns") end
  })

  -- Greeter window
  use({
    "goolord/alpha-nvim",
    config = function() require("config.plugins.alpha") end
  })

  -- Paste images in markdown files
  use({
    "ferrine/md-img-paste.vim",
    config = function() require("config.plugins.md-img-paste") end
  })

  use({
    "jbyuki/nabla.nvim",
    config = function() require("config.plugins.nabla") end
  })

  -- Personal plugins
  use({
    "propet/toggle-fullscreen.nvim",
    config = function() require("config.plugins.toggle-fullscreen") end
  })

  use({
    "propet/colorscheme-persist.nvim",
    config = function() require("config.plugins.colorscheme-persist") end,
    requires = {
      "nvim-telescope/telescope-dap.nvim"
    }
  })


  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
