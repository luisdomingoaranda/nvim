local which_key = require("which-key")

which_key.add({
  { "<leader>d",  group = "dap" },
  { "<leader>l",  group = "lsp" },
  { "<leader>h",  group = "hex-bin" },
  { "<leader>g",  group = "git" },
  { "<leader>m",  group = "markdown" },
  { "<leader>p",  group = "packer" },
  { "<leader>lw", group = "workspace" },
  { "<leader>s",  group = "search" },
})
