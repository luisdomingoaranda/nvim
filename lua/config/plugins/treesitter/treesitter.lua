require("nvim-treesitter.configs").setup({
  -- A list of parser names, or "all" (the four listed parsers should always be installed)
  ensure_installed = {
    "c",
    "lua",
    "nix",
    "typescript",
    "tsx",
    "markdown",
    "latex",
    -- "cpp",
    -- "css",
    -- "html",
    -- "javascript",
    -- "json",
    -- "php",
    -- "python",
    -- "vue",
  },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Don't install new parsers automatically
  auto_install = false,

  -- List of parsers to ignore installing (for "all")
  ignore_install = { "javascript" },

  highlight = {
    enable = true,

    -- Disable slow treesitter highlight for large files
    disable = function(_, buf)
        local max_filesize = 100 * 1024 -- 100 KB
        local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
        if ok and stats and stats.size > max_filesize then
            return true
        end
    end,
  },
})
