-- Allow you to delete a whole for loop doing `dal`
-- Or change the body of a function doing `cif`
-- Or select the body of a whole class doing `vac`

require("nvim-treesitter.configs").setup({
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ["ac"] = { query = "@call.outer", desc = "Select outer part of a function call region" },
        ["ic"] = { query = "@call.inner", desc = "Select inner part of a function call region" },
        ["af"] = { query = "@function.outer", desc = "Select outer part of a function region" },
        ["if"] = { query = "@function.inner", desc = "Select inner part of a function region" },
        ["ai"] = { query = "@conditional.outer", desc = "Select outer part of a conditional region" },
        ["ii"] = { query = "@conditional.inner", desc = "Select inner part of a conditional region" },
        ["al"] = { query = "@loop.outer", desc = "Select outer part of a loop region" },
        ["il"] = { query = "@loop.inner", desc = "Select inner part of a loop region" },
        ["ao"] = { query = "@comment.outer", desc = "Select outer part of a comment region" },
        ["io"] = { query = "@comment.inner", desc = "Select inner part of a comment region" },
        ["as"] = { query = "@class.outer", desc = "Select outer part of a class region" },
        ["is"] = { query = "@class.inner", desc = "Select inner part of a class region" },
      },
    },
  },
})
