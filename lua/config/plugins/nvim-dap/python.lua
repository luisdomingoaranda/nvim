return {

  adapter = {
    key = "python", -- adapter name
    value = { -- dap.adapters[key] = value
      type = 'executable',
      command = vim.fn.exepath('python'),
      args = { '-m', 'debugpy.adapter' }
    }
  },

  configuration = {
    key = "python", -- language name
    value = { -- dap.configurations[key] = value
        type = 'python', -- adapter name
        request = 'launch',
        name = "Launch file",
        program = "${file}",
        pythonPath = vim.fn.exepath('python')
    }
  }

}
