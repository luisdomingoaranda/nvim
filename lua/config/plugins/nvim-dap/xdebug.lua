return {

  adapter = {
    key = "php", -- adapter name
    value = { -- dap.adapters[key] = value
      type = 'executable',
      command = 'node',
      args = { '/home/luis/dev/vscode-php-debug/out/phpDebug.js' }
    }
  },

  configuration = {
    key = "php", -- language name
    value = { -- dap.configurations[key] = value
        type = 'php',
        request = 'launch',
        name = 'Listen for Xdebug',
        port = 9003,
        log = true,
        pathMappings = {
          ['/var/www/html/'] = "${workspaceFolder}",
        },
    }
  }

}
