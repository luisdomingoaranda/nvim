local M = {}

local _instructions = {
  ["H"] = function() require("dap").up() end,
  ["h"] = function() require("dap").step_out() end,
  ["j"] = function() require("dap").step_over() end,
  ["k"] = function() require("dap").step_back() end,
  ["l"] = function() require("dap").step_into() end,
  ["L"] = function() require("dap").down() end,
  ["r"] = function() require("dap").run_last() end,
  ["c"] = function() require("dap").continue() end,
  ["b"] = function() require("dap").toggle_breakpoint() end,
}

function M.enter()
  require('libmodal').mode.enter("STEP DEBUGGER", _instructions)
end

return M
