local dapui = require("dapui")

-- Keymap
vim.api.nvim_set_keymap(
  "n",
  "<leader>du",
  ":lua require('dapui').toggle()<CR>",
  { noremap = true, silent = true }
)

vim.api.nvim_set_keymap(
  "n",
  "<leader>di",
  ":lua require('dapui').float_element(_, { enter = true })<CR>",
  { noremap = true, silent = true }
)

-- Initialize config
dapui.setup({
  mappings = {
    -- defaults
    -- remove = "d", -- delete
    -- edit = "e", -- edit values
    -- repl = "r", -- send variables to repl
    -- toggle = "t", -- toggle breakpoints
    expand = { "o" },
  }
})
