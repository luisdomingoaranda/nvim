local utils = require("config.utils")
local dap = require("dap")

-- keymaps (Mimic gdb shortcuts)
utils.keymap_set("n", "<leader>db", require('dap').toggle_breakpoint, { desc = "Breakpoint (break)" })
utils.keymap_set("n", "<leader>dB", function() require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: ')) end, { desc = "Conditional breakpoint" })
utils.keymap_set("n", "<leader>dc", require('dap').continue, { desc = "Continue" })
utils.keymap_set("n", "<leader>df", require('dap').step_out, { desc = "Step out (finish)" })
utils.keymap_set("n", "<leader>dh", require('dap.ui.widgets').hover, { desc = "Hover (value info)" })
utils.keymap_set("n", "<leader>dl", function() require('dap').set_breakpoint(nil, nil, vim.fn.input('Log point message: ')) end, { desc = "Log message" })
utils.keymap_set("n", "<leader>dm", require('config.plugins.nvim-dap.debug-mode').enter, { desc = "Stepper mode" })
utils.keymap_set("n", "<leader>dn", require('dap').step_over, { desc = "Step over (next)" })
utils.keymap_set("n", "<leader>do", function() require('dap').repl.open({}, 'vsplit') end, { desc = "Open" })
utils.keymap_set("n", "<leader>dp", function() local widgets=require('dap.ui.widgets'); widgets.centered_float(widgets.scopes) end, { desc = "Scopes info" })
utils.keymap_set("n", "<leader>dr", require('dap').run_last, { desc = "Run (restart)" })
utils.keymap_set("n", "<leader>ds", require('dap').step_into, { desc = "Step into (step)" })
utils.keymap_set("n", "<leader>dt", require('dap').terminate, { desc = "Terminate" })


-- Setup adapters
local dap_langs = {
  {
    language = "python",
    adapter = require("config.plugins.nvim-dap.python").adapter,
    configuration = require("config.plugins.nvim-dap.python").configuration,
  },
  {
    language = "c",
    adapter = require("config.plugins.nvim-dap.cpptools").adapter,
    configuration = require("config.plugins.nvim-dap.cpptools").configuration,
  },
  {
    language = "php",
    adapter = require("config.plugins.nvim-dap.xdebug").adapter,
    configuration = require("config.plugins.nvim-dap.xdebug").configuration,
  },
}

for _, dap_lang in ipairs(dap_langs) do
  -- Configuration goes wrapped inside a table because for each language
  -- you can have various configurations, one for each adapter
  -- But here we assume we're just gonna have one configuration per language
  dap.configurations[dap_lang.configuration.key] = { dap_lang.configuration.value };
  -- For each adapter there's just one setting
  dap.adapters[dap_lang.adapter.key] = dap_lang.adapter.value;
end
