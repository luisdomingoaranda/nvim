return {

  adapter = {
    key = "cppdbg", -- adapter name
    value = { -- dap.adapters[key] = value
      type = 'executable',
      -- command = os.getenv("OPENDEBUGAD7")
      command = "/home/luis/Downloads/cpptools/extension/debugAdapters/bin/OpenDebugAD7"
    }
  },

  configuration = {
    key = "c",
    value = {
      name = 'Attach to gdbserver :3333',
      type = 'cppdbg',
      request = 'launch',
      MIMode = 'gdb',
      miDebuggerServerAddress = ':3333',
      miDebuggerPath = vim.fn.exepath('arm-none-eabi-gdb'),
      cwd = '${workspaceFolder}',
      program = function()
        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
      end,
    },
  }

}

