local persist_colorscheme = require("colorscheme-persist")

-- Setup
persist_colorscheme.setup({
  file_path = "/home/luis/.nvim.colorscheme-persist",
  fallback = "blue",
  picker_opts = require("telescope.themes").get_ivy(),
})

-- Get stored colorscheme
local colorscheme = persist_colorscheme.get_colorscheme()

-- Set colorscheme
local ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not ok then
  vim.notify("colorscheme " .. colorscheme .. " not found")
end

vim.keymap.set(
  "n",
  "<leader>sc",
  require("colorscheme-persist").picker,
  { noremap = true, silent = true, desc = "colorscheme-persist" }
)
