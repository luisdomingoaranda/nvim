---------
-- Config
---------
local signs = {
  { name = "DiagnosticSignError", text = "" },
  { name = "DiagnosticSignWarn", text = "" },
  { name = "DiagnosticSignHint", text = "" },
  { name = "DiagnosticSignInfo", text = "" },
}

for _, sign in ipairs(signs) do
  vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
end

local config = {
  -- disable virtual text
  virtual_text = false,
  -- show signs
  signs = {
    active = signs,
  },
  update_in_insert = true,
  underline = true,
  severity_sort = true,
  float = {
    focusable = true,
    style = "minimal",
    border = "rounded",
    source = "always",
    header = "",
    prefix = "",
  },
}

vim.diagnostic.config(config)

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
  border = "rounded",
})

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
  border = "rounded",
})


----------------
-- NVIM LSP
----------------
local nvim_lsp = require("lspconfig")

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

local on_attach = function (client, bufnr)
  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  buf_set_keymap("n", "<leader>lc", "<cmd>lua vim.lsp.buf.code_action()<CR>", { desc = "Code action" })
  buf_set_keymap("n", "<leader>lD", "<cmd>lua vim.lsp.buf.declaration()<CR>", { desc = "Declaration" })
  buf_set_keymap("n", "<leader>ld", "<cmd>lua vim.lsp.buf.definition()<CR>", { desc = "Definition" })
  buf_set_keymap("n", "<leader>lh", "<cmd>lua vim.lsp.buf.hover()<CR>", { desc = "Hover" })
  buf_set_keymap("n", "<leader>li", "<cmd>lua vim.lsp.buf.implementation()<CR>", { desc = "Implementation" })
  buf_set_keymap("n", "<leader>ls", "<cmd>lua vim.lsp.buf.signature_help()<CR>", { desc = "Signature" })
  buf_set_keymap("n", "<leader>lwa", "<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>", { desc = "Add workspace folder" })
  buf_set_keymap("n", "<leader>lwr", "<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>", { desc = "Remove workspace folder" })
  buf_set_keymap("n", "<leader>lwl", "<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>", { desc = "List workspace folders" })
  buf_set_keymap("n", "<leader>lt", "<cmd>lua vim.lsp.buf.type_definition()<CR>", { desc = "Type definition" })
  buf_set_keymap("n", "<leader>lr", "<cmd>lua vim.lsp.buf.rename()<CR>", { desc = "Rename" })
  buf_set_keymap("n", "<leader>le", "<cmd>lua vim.lsp.buf.references()<CR>", { desc = "References" })
  buf_set_keymap("n", "<leader>lm", "<cmd>lua vim.diagnostic.open_float()<CR>", { desc = "Open float" })
  buf_set_keymap("n", "<leader>lp", "<cmd>lua vim.diagnostic.goto_prev()<CR>", { desc = "Go to previous" })
  buf_set_keymap("n", "<leader>ln", "<cmd>lua vim.diagnostic.goto_next()<CR>", { desc = "Go to next" })
  buf_set_keymap("n", "<leader>lo", "<cmd>lua vim.diagnostic.setloclist()<CR>", { desc = "Set loc list" })
  buf_set_keymap("n", "<leader>lf", "<cmd>lua vim.lsp.buf.format({ async = true })<CR>", { desc = "Formatting" })

  -- Set autocommands conditional on server_capabilities
  if client.server_capabilities.document_highlight then
    vim.api.nvim_exec(
      [[
        augroup lsp_document_highlight
          autocmd! * <buffer>
          autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
          autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
        augroup END
      ]],
      false
    )
  end
end


----------------
-- Setup servers
----------------
-- See possible names and server settings
-- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
local servers = {
  { name = "clangd", opts_override = "config.plugins.lsp.clangd" },
  { name = "tsserver", opts_override = "" },
  { name = "intelephense", opts_override = "" },
  { name = "vuels", opts_override = "" },
  { name = "lua_ls", opts_override = "config.plugins.lsp.lua_ls" },
  { name = "pyright", opts_override = "" },
  { name = "rnix", opts_override = "" },
  { name = "texlab", opts_override = "" },
}
for _, server in ipairs(servers) do
  local opts = {
    capabilities = capabilities,
    on_attach = on_attach
  }

  -- Add options object if provided path to file
  if server.opts_override ~= nil and server.opts_override ~= "" then
    -- Override opts with opts_override
    local opts_override = require(server.opts_override)
    for key, value in pairs(opts_override) do
      opts[key] = value
    end
  end

  nvim_lsp[server.name].setup(opts)
end
