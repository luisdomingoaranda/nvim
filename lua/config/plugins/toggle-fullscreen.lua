local utils = require("config.utils")

utils.keymap_set(
  "n",
  "<leader>f",
  ":lua require('toggle-fullscreen'):toggle_fullscreen()<CR>",
  { desc = "toggle-fullscreen" }
)
