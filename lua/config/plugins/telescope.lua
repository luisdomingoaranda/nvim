local utils = require("config.utils")
local actions = require("telescope.actions")
local telescope = require("telescope")

telescope.setup({
  defaults = {

    prompt_prefix = " ",
    selection_caret = " ",
    path_display = { shorten = 3 },

    mappings = {
      i = {
        ["<C-j>"] = actions.cycle_history_next,
        ["<C-k>"] = actions.cycle_history_prev,

        ["<C-n>"] = actions.move_selection_next,
        ["<C-p>"] = actions.move_selection_previous,

        ["<C-c>"] = actions.close,

        ["<Down>"] = actions.move_selection_next,
        ["<Up>"] = actions.move_selection_previous,

        ["<CR>"] = actions.select_default,
        ["<C-x>"] = actions.select_horizontal,
        ["<C-v>"] = actions.select_vertical,
        ["<C-t>"] = actions.select_tab,

        ["<C-u>"] = actions.preview_scrolling_up,
        ["<C-d>"] = actions.preview_scrolling_down,

        ["<PageUp>"] = actions.results_scrolling_up,
        ["<PageDown>"] = actions.results_scrolling_down,

        ["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
        ["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,
        ["<C-q>"] = actions.send_to_qflist + actions.open_qflist,
        ["<M-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
        ["<C-l>"] = actions.complete_tag,
        ["<C-_>"] = actions.which_key, -- keys from pressing <C-/>
      },

      n = {
        ["<esc>"] = actions.close,
        ["<CR>"] = actions.select_default,
        ["<C-x>"] = actions.select_horizontal,
        ["<C-v>"] = actions.select_vertical,
        ["<C-t>"] = actions.select_tab,

        ["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
        ["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,
        ["<C-q>"] = actions.send_to_qflist + actions.open_qflist,
        ["<M-q>"] = actions.send_selected_to_qflist + actions.open_qflist,

        ["j"] = actions.move_selection_next,
        ["k"] = actions.move_selection_previous,
        ["H"] = actions.move_to_top,
        ["M"] = actions.move_to_middle,
        ["L"] = actions.move_to_bottom,

        ["<Down>"] = actions.move_selection_next,
        ["<Up>"] = actions.move_selection_previous,
        ["gg"] = actions.move_to_top,
        ["G"] = actions.move_to_bottom,

        ["<C-u>"] = actions.preview_scrolling_up,
        ["<C-d>"] = actions.preview_scrolling_down,

        ["<PageUp>"] = actions.results_scrolling_up,
        ["<PageDown>"] = actions.results_scrolling_down,

        ["?"] = actions.which_key,
      },
    },
  },
  pickers = {
    -- Default configuration for builtin pickers goes here:
    -- picker_name = {
    --   picker_config_key = value,
    --   ...
    -- }
    -- Now the picker_config_key will be applied every time you call this
    -- builtin picker
  },
  extensions = {
    -- Your extension configuration goes here:
    -- extension_name = {
    --   extension_config_key = value,
    -- }
    -- please take a look at the readme of the extension you want to configure
  },
})


-- Mappings
utils.keymap_set("n", "<C-p>", "<cmd>Telescope find_files<cr>", { desc = "Git files" })
utils.keymap_set("n", "<leader>sb", "<cmd>Telescope buffers<cr>", { desc = "Buffers" })
utils.keymap_set("n", "<leader>sf", "<cmd>Telescope find_files no_ignore=true<cr>", { desc = "Files" })
utils.keymap_set("n", "<leader>sg", "<cmd>Telescope live_grep<cr>", { desc = "Grep" })
utils.keymap_set("n", "<leader>sh", "<cmd>Telescope help_tags<cr>", { desc = "Help" })
utils.keymap_set("n", "<leader>sm", "<cmd>Telescope man_pages<cr>", { desc = "Man pages" })
utils.keymap_set("n", "<leader>sr", "<cmd>Telescope registers<cr>", { desc = "Registers" })
utils.keymap_set("n", "<leader>sk", "<cmd>Telescope utils.keymap_sets<cr>", { desc = "Keymaps" })
utils.keymap_set("n", "<leader>sC", "<cmd>Telescope commands<cr>", { desc = "Commands" })


-- Extensions
telescope.load_extension("dap")
utils.keymap_set("n", "<leader>sdc", require('telescope').extensions.dap.commands, { desc = "Commands" }) -- nvim-dap commands
utils.keymap_set("n", "<leader>sdb", require('telescope').extensions.dap.list_breakpoints, { desc = "Breakpoints" })
utils.keymap_set("n", "<leader>sdf", require('telescope').extensions.dap.frames, { desc = "Frames" })
utils.keymap_set("n", "<leader>sdv", require('telescope').extensions.dap.variables, { desc = "Variables" })
