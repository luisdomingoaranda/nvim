local M = {}

-- Add a new |mapping|.
-- Like vim.keymap.set, but with different default values
--
-- @param mode string|table    Same mode short names as |nvim_set_keymap()|.
-- @param lhs string           Left-hand side |{lhs}| of the mapping.
-- @param rhs string|function  Right-hand side |{rhs}| of the mapping. Can also be a Lua function.
-- @param opts table           A table with the following posible properties
--                             - noremap (boolean): defaults to false
--                             - silent (boolean): defaults to true
--                             - desc (string): description. defaults to nil
function M.keymap_set(mode, lhs, rhs, opts)
  -- Required inputs
  vim.validate({
    mode = { mode, "string" },
    lhs = { lhs, "string" },
    rhs = { rhs, { "string", "function" } }
  })
  -- Optional inputs
  opts = opts or {}
  local default_opts = {
    noremap = true,
    silent = true
  }
  -- Merge defaults with input opts
  opts = vim.tbl_extend("force", default_opts, opts)
  -- Set keymap
  vim.keymap.set(mode, lhs, rhs, opts)
end

return M
