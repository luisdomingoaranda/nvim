-------------------
-- Global functions
-------------------

-- Dump lua value
d = function(v)
  print(vim.inspect(v))
  return v
end

-- Reload cached module
-- e.g. `:lua R("config")`
local ok, plenary = pcall(require, "plenary")
if ok then
  RELOAD = plenary.reload.reload_module

  R = function(name)
    RELOAD(name)
    return require(name)
  end
end

-------------------
-- Module functions
-------------------
local M = {}
return M
