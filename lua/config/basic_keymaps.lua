-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

local utils = require("config.utils")
local nonsilent_opts = { silent = false }

-- Remap space as leader key
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Stay in indent mode after indenting a block
utils.keymap_set("v", "<", "<gv")
utils.keymap_set("v", ">", ">gv")

-- Copy and paste to system clipboard
utils.keymap_set("v", "<C-c>", '"+y')
utils.keymap_set("v", "<C-v>", '"+p')

-- Better window navigation
utils.keymap_set("n", "<C-h>", "<C-w>h")
utils.keymap_set("n", "<C-j>", "<C-w>j")
utils.keymap_set("n", "<C-l>", "<C-w>l")
utils.keymap_set("n", "<C-k>", "<C-w>k")

-- Navigate buffers
utils.keymap_set("n", "<S-l>", ":bnext<CR>")
utils.keymap_set("n", "<S-h>", ":bprevious<CR>")

-- Resize with arrows
utils.keymap_set("n", "<C-Up>", ":resize -2<CR>")
utils.keymap_set("n", "<C-Down>", ":resize +2<CR>")
utils.keymap_set("n", "<C-Left>", ":vertical resize -2<CR>")
utils.keymap_set("n", "<C-Right>", ":vertical resize +2<CR>")

-- Navigate buffers
utils.keymap_set("n", "<S-l>", ":bnext<CR>")
utils.keymap_set("n", "<S-h>", ":bprevious<CR>")

-- Move text up and down
utils.keymap_set("n", "<A-j>", "<Esc>:m .+1<CR>==")
utils.keymap_set("n", "<A-k>", "<Esc>:m .-2<CR>==")
utils.keymap_set("i", "<A-j>", "<Esc>:m .+1<CR>==gi")
utils.keymap_set("i", "<A-k>", "<Esc>:m .-2<CR>==gi")
utils.keymap_set("x", "<A-j>", ":move '>+1<CR>gv-gv")
utils.keymap_set("x", "<A-k>", ":move '<-2<CR>gv-gv")

-- Close terminal
utils.keymap_set("t", "<C-[>", "<C-\\><C-n>")
utils.keymap_set("t", "<esc>", "<C-\\><C-n>")

-- Remove search highlight with <esc>
utils.keymap_set("n", "<esc>", ":noh<cr>")

-- Replace
utils.keymap_set("n", "R", ":%s//g<LEFT><LEFT>", nonsilent_opts)
utils.keymap_set("x", "R", ":s//g<LEFT><LEFT>", nonsilent_opts)

-- Plugin development
utils.keymap_set(
  "n",
  "<leader>x",
  ":w | :source %<CR>",
  { silent = false, desc = "execute (save & source)" }
)

-- Open url in browser
utils.keymap_set("n", "gx", ":!xdg-open <C-r><C-a><CR>")

-- Edit binaries
utils.keymap_set("n", "<leader>hr", ":%!xxd -r <CR> | :%!xxd <CR>", { desc = "Reload hex" })
utils.keymap_set("n", "<leader>hb", ":%!xxd -r <CR>", { desc = "Hex to binary" })
utils.keymap_set("n", "<leader>he", ":%!xxd <CR>", { desc = "Binary to hex" })

-- Open help your word under cursor
utils.keymap_set("n", "<leader>lH", "viwy | :help <c-r>0<cr>", { desc = "Help" }) -- yank inner word, then do ":help <c-r>0<cr>" same as ":help <paste><enter>"
