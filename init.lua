-- Reload every module at init
for k in pairs(package.loaded) do
  if k:match("^config") then
    package.loaded[k] = nil
  end
end

require("config")
require("config.options")
require("config.basic_keymaps")
require("config.plugins.termdebug")
require("config.plugins")
