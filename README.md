# Neovim config files

Installed with its dependencies using home-manager (nix)

If plugins aren't installed on first use, you may need to execute:

```
:PackerSync
```


## Packages used:

- [which-key](https://github.com/folke/which-key.nvim)  
    Shows your keymaps when typing the leader key (e.g. <space>)

- [nvim-dap](https://github.com/mfussenegger/nvim-dap)  
    Debug Adapter Protocol client implementation for Neovim

- [nvim-libmodal](https://github.com/Iron-E/nvim-libmodal)  
    Create new vim modes to namespace your keymaps

- [guess-indent](https://github.com/NMAC427/guess-indent.nvim)  
    Instead of writing ftplugin files for setting the indentation for each
    filtype like

    ```
    # ~/.config/nvim/ftplugin/php.lua
    vim.bo.tabstop = 4
    vim.bo.shiftwidth = 4
    vim.bo.softtabstop = 4
    ```

    Automatically guess the indentation of the current file

- [null-ls](https://github.com/jose-elias-alvarez/null-ls.nvim)  
    Feed neovim lsp client with non-lsp sources.  
    e.g. format a file like calling the lsp format function, but use instead
    a third party formatter.

- [bufferline](https://github.com/akinsho/bufferline.nvim)  
    Shows open buffers as tabs.  
    Switch between those with `<S-h>` `<S-l>`.  
    Close current buffer with `:bd`

- [obsession](https://github.com/tpope/vim-obsession)  
    Source your session file with `vim -S` and obsession will keep it updated.
    To initially create the `Session.vim` file, do: `:Obsession` or `:mksession`

- [packer](https://github.com/wbthomason/packer.nvim)  
    Neovim Package manager written in Lua

- [popup.nvim](https://github.com/nvim-lua/popup.nvim)  
    Popups for neovim. Required as a dependency for multiple other packages

- [plenary.nvim](https://github.com/nvim-lua/plenary.nvim)  
    Lua library

- [autopairs](https://github.com/windwp/nvim-autopairs)  
    Automatically close pairs of brackets/quotes, while typing

- [comment](https://github.com/numToStr/Comment.nvim)  
    Comment and uncomment lines of code (mappings "gc" and "gcc")

- [nvim-surround](https://github.com/kylechui/nvim-surround)  
    Create/change/delete surrounds like "", '', (), etc

- [nvim-tree](https://github.com/kyazdani42/nvim-tree.lua)  
    Neovim file explorer sidebar

- [gitsigns](https://github.com/lewis6991/gitsigns.nvim)  
    Git plugin for neovim

- [alpha-vim](https://github.com/goolord/alpha-nvim)  
    Greeter window with some shortcuts

- [toggle-fullscreen](https://github.com/propet/toggle-fullscreen.nvim)  
    Toggle a buffer in fullscreen mode

- [colorscheme-persist](https://github.com/propet/colorscheme-persist.nvim)  
    Like `:Telescope colorscheme` but saving the selection to a file so the
    colorscheme can persist the next time you open neovim. Without having to
    manually modify your config files.

- [telescope](https://github.com/nvim-telescope/telescope.nvim)  
    Customazible Fuzzy finder over lists

- [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)  
    Better syntax coloring

- [nvim-treesitter-textobjects](https://github.com/nvim-treesitter/nvim-treesitter-textobjects)
    Use treesitter selections to do things like

    ```
    caf -> change around function
    dic -> delete inner class
    sil -> select inner loop
    dai -> delete around if condition
    ```

- [lspconfig](https://github.com/neovim/nvim-lspconfig)  
    Neovim lsp configurations for various servers

- [md-img-paste](ferrine/md-img-paste.vim)  
    Paste images from clipboard to create a markdown image link.
    Mapped to `<leader>mp`.


## Colorschemes

- [gruvbox-baby](https://github.com/luisiacc/gruvbox-baby)
- [vim-moonfly-colors](https://github.com/bluz71/vim-moonfly-colors)
- [space-vim](https://github.com/Th3Whit3Wolf/space-nvim)
- [roshivim-cs](https://github.com/shaeinst/roshnivim-cs)
- [tokyo-night](https://github.com/folke/tokyonight.nvim)
- [sonokai](https://github.com/sainnhe/sonokai)
- [everforest](https://github.com/sainnhe/everforest)


## Also:

- [awesome-neovim](https://github.com/rockerBOO/awesome-neovim/blob/main/README.md)  
    Collection of other popular neovim plugins
